#include"Random.h"
#include "Pedestrian.h"

double getRandom(){
    static double lastTime = -1;
    if(lastTime != time(NULL)){
        lastTime = time(NULL);
        srand(lastTime);
    }
    return rand()%100/100.0;
}

int getRandomInteger(int min, int max){
    return (getRandom()*(max-min))+min;
}

double computeDistance(Pedestrian p1, Pedestrian p2){
    return sqrt(pow(p1.getX()-p2.getX(),2)+pow(p1.getY()-p2.getY(),2));
}

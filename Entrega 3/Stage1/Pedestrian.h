#ifndef PEDESTRIAN_H
#define PEDESTRIAN_H
#include <string>
#include <QRandomGenerator>
#include "PedestrianView.h"
using namespace std;
class Comuna;
class Pedestrian {
private:
    double x, y, speed, angle, deltaAngle;
    double x_tPlusDelta, y_tPlusDelta;
    Comuna &comuna;
    QRandomGenerator myRand; // see https://doc.qt.io/qt-5/qrandomgenerator.html
    PedestrianView pedestrianView;
public:
    Pedestrian(Comuna &com, double speed, double deltaAngle);
    static string getStateDescription() {
        return "x, \ty";
    };
    string getState() const;
    void computeNextState(double delta_t);
    void updateState();
    double getX();
    double getY();
    void reespawn();
    void updateView();
};

#endif // PEDESTRIAN_H

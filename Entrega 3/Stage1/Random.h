#ifndef RANDOM_H
#define RANDOM_H

//#include"Vector.h"
#include<time.h>
#include<stdlib.h>
#include<cmath>

int getRandomInteger(int min, int max); // Crea un número aleatorio en un intervalo.
double getRandom(); //Crea un número aleatorio entre cero y uno.
//double computeDistance(Vector p1,Vector p2);

#endif // RANDOM_H

#ifndef SIMULATORCONFIG_H
#define SIMULATORCONFIG_H
#include "Simulator.h"

class SimulatorConfig{
public:
    static double N, I, I_TIME;
    static double WIDTH, LENGTH;
    static double SPEED, DELTA_T, DELTA_THETA;
    static double D, M, P0, P1, P2;
    static double NUM_VAC, VAC_SIZE, VAC_TIME;

    //SimulatorConfig(Scanner s);
};

#endif // SIMULATORCONFIG_H

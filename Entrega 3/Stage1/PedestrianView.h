#ifndef PEDESTRIANVIEW_H
#define PEDESTRIANVIEW_H


#include "Comuna.h"
#include "Pedestrian.h"
#include <QRect>


class PedestrianView{
    private:
        Pedestrian person;
        QRect view;
        double SIZE=5;
    public:

        PedestrianView(Comuna comuna, Pedestrian p);
        void update();

};






#endif // PEDESTRIANVIEW_H

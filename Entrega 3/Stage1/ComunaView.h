#ifndef COMUNAVIEW_H
#define COMUNAVIEW_H
#include "Comuna.h"

class ComunaView : public Comuna{
private:
    Comuna comuna;

public:
    ComunaView(Comuna c);
    void update();


};
#endif // COMUNAVIEW_H

#ifndef COMUNA_H
#define COMUNA_H
#include "Pedestrian.h"
#include <QRect>
#include <string>
#include "ComunaView.h"
using namespace std;
class Comuna {
private:
    Pedestrian * pPerson;
    QRect territory; // Alternatively: double width, length;
    // but more methods would be needed.
    double width, length;
    ComunaView view;
public:
    Comuna();
    Comuna(double width=1000, double length=1000);
    double getWidth() const;
    double getHeight() const;
    void setPerson(Pedestrian &person);
    void computeNextState (double delta_t);
    void updateState ();
    static string getStateDescription();
    string getState() const;
    double getView();
    void updateView();
 };

#endif // COMUNA_H

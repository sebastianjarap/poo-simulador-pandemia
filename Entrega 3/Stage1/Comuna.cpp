#include "Comuna.h"
#include "Pedestrian.h"

Comuna::Comuna(){
}
Comuna::Comuna(double width, double length) : territory(0,0,width,length){
    // pPerson= new Pedestrian(this, speed,deltaAngle);

    this->width=width;
    this->length=length;

}
double Comuna::getWidth() const {
    return width;
}
double Comuna::getHeight() const {
    return length;
}
/*void Comuna::setPerson(Pedestrian & person){
   //....
}*/
void Comuna::computeNextState (double delta_t) {
   pPerson->computeNextState(delta_t);
}
void Comuna::updateState () {
  pPerson->updateState();
}

void Comuna::updateView(){
        pPerson->updateView();
        view.update();
    }
double Comuna::getView() {
    return view;
}

/*string Comuna::getStateDescription(){
    return //....
}
string Comuna::getState() const{
    return //...
}*/

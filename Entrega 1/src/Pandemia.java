
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * TODO_javadoc.
 */
public class Pandemia {

    /** TODO_javadoc. */
    private static int simulationDuration;

    /** TODO_javadoc. */
    private static int cantIndividuos;

    /** TODO_javadoc. */
    private static int cantInfectados;

    /** TODO_javadoc. */
    private static int iTime;

    /** TODO_javadoc. */
    private static double comunaWidth;

    /** TODO_javadoc. */
    private static double comunaLength;

    /** TODO_javadoc. */
    private static double speed;

    /** TODO_javadoc. */
    private static double delta_t;

    /** TODO_javadoc. */
    private static double deltaAngle;

    /** TODO_javadoc. */
    private static int distanciaContagio;

    /** TODO_javadoc. */
    private static double fraccionUsoMask;

    /** TODO_javadoc. */
    private static double p0;

    /** TODO_javadoc. */
    private static double p1;

    /** TODO_javadoc. */
    private static double p2;

    /** TODO_javadoc. */
    private static int numVac;

    /** TODO_javadoc. */
    private static double vacSize;

    /** TODO_javadoc. */
    private static int vacTime;

    /*******************************************************************************************************
     * main
     *******************************************************************************************************/
    
    public static void main(String[] args) throws IOException {

        if (args.length < 1) {

          //System.out.println("Usage: java PandemiaMain <configurationFile.txt> [R_time]");

            System.exit(-1);
        }

        Scanner s = new Scanner(new File(args[0]));

        s.useLocale(Locale.US); // punto como separador decimal

      //System.out.println("File: " + args[0]);

        getParameters(s);

        int rTime = iTime;

        if (args.length > 1) {
            rTime = Integer.parseInt(args[1]);
        }

        /////////////////////////////////////////////////////////////////////

        double samplingTime = 1.0;    // 1 [s]

      //System.out.println("samplingTime     : " + samplingTime);
      //System.out.println("R_time           : " + rTime);

        //

        Comuna    comuna = new Comuna(comunaWidth, comunaLength, cantIndividuos, cantInfectados, iTime, rTime, speed, deltaAngle, distanciaContagio, fraccionUsoMask, p0, p1, p2, numVac, vacSize);
        Simulador sim    = new Simulador(System.out, comuna);

        sim.simulate(delta_t, simulationDuration, samplingTime, vacTime);
    }

    /*******************************************************************************************************
     * getParameters
     *******************************************************************************************************/
   
    public static void getParameters(Scanner s) {

        simulationDuration = s.nextInt();
        cantIndividuos     = s.nextInt();
        cantInfectados     = s.nextInt();
        iTime              = s.nextInt();

      //System.out.println("simulationDuration: " + simulationDuration);
      //System.out.println("cantIndividuos    : " + cantIndividuos);
      //System.out.println("cantInfectados    : " + cantInfectados);
      //System.out.println("iTime             : " + iTime);

        s.nextLine();

        comunaWidth  = s.nextDouble();
        comunaLength = s.nextDouble();

      //System.out.println("comunaWidth       : " + comunaWidth);
      //System.out.println("comunaLength      : " + comunaLength);

        s.nextLine();

        speed      = s.nextDouble();
        delta_t    = s.nextDouble();
        deltaAngle = s.nextDouble();

      //System.out.println("speed             : " + speed);
      //System.out.println("delta_t           : " + delta_t);
      //System.out.println("deltaAngle        : " + deltaAngle);

        s.nextLine();

        distanciaContagio = s.nextInt();
        fraccionUsoMask   = s.nextDouble();
        p0                = s.nextDouble();
        p1                = s.nextDouble();
        p2                = s.nextDouble();

      //System.out.println("distanciaContagio : " + distanciaContagio);
      //System.out.println("fraccionUsoMask   : " + fraccionUsoMask);
      //System.out.println("p0                : " + p0);
      //System.out.println("p1                : " + p1);
      //System.out.println("p2                : " + p2);

        s.nextLine();

        numVac  = s.nextInt();
        vacSize = s.nextDouble();
        vacTime = s.nextInt();

      //System.out.println("numVac            : " + numVac);
      //System.out.println("vacSize           : " + vacSize);
      //System.out.println("vacTime           : " + vacTime);
    }
}

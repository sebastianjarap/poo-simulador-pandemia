import java.awt.geom.Rectangle2D;

import java.util.ArrayList;


public class Comuna {

    /** TODO_javadoc. */
    private Rectangle2D territory = null;

    /** TODO_javadoc. */
    public ArrayList<Individuo> personas = null;

    /** TODO_javadoc. */
    public ArrayList<Vacunatorio> vacunatorios = null;

    //

    /** TODO_javadoc. */
    private double width;

    /** TODO_javadoc. */
    private double length;

    /** TODO_javadoc. */
    private int cantIndividuos;

    /** TODO_javadoc. */
    private int cantInfectados;

    /** TODO_javadoc. */
    private int iTime;

    /** TODO_javadoc. */
    private int rTime;

    /** TODO_javadoc. */
    private double speed;

    /** TODO_javadoc. */
    private double deltaAngle;

    /** TODO_javadoc. */
    private int distanciaContagio;

    /** TODO_javadoc. */
    private double fraccionUsoMask;

    /** TODO_javadoc. */
    private double p0;

    /** TODO_javadoc. */
    private double p1;

    /** TODO_javadoc. */
    private double p2;

    /** TODO_javadoc. */
    private int numVac;

    /** TODO_javadoc. */
    private double vacSize;

    /*******************************************************************************************************
     * Comuna
     *******************************************************************************************************/
    
    public Comuna(double width, double length, int cantIndividuos, int cantInfectados, int iTime, int rTime, double speed, double deltaAngle, int distanciaContagio, double fraccionUsoMask, double p0, double p1, double p2, int numVac, double vacSize) {

        this.width             = width;
        this.length            = length;
        this.cantIndividuos    = cantIndividuos;
        this.cantInfectados    = cantInfectados;
        this.iTime             = iTime;
        this.rTime             = rTime;
        this.speed             = speed;
        this.deltaAngle        = deltaAngle;
        this.distanciaContagio = distanciaContagio;
        this.fraccionUsoMask   = fraccionUsoMask;
        this.p0                = p0;
        this.p1                = p1;
        this.p2                = p2;
        this.numVac            = numVac;
        this.vacSize           = vacSize;

        vacunatorios = new ArrayList<Vacunatorio>();
        territory    = new Rectangle2D.Double(0, 0, width, length);
    }

    /*******************************************************************************************************
     * computeNextState
     *******************************************************************************************************/
  
    public void computeNextState(double t, double delta_t) {

        for (int i = 0; i < personas.size(); i++) {
            personas.get(i).computeNextState(t, delta_t);
        }
    }

    /*******************************************************************************************************
     * genVacunatorios
     *******************************************************************************************************/

    public void genVacunatorios() {

        int cant = 0;

        while (cant < numVac) {

            double x = Math.random() * (width  - vacSize);
            double y = Math.random() * (length - vacSize);

            if (!seTraslapaConOtros(x, y)) {

                vacunatorios.add(new Vacunatorio(x, y, vacSize));

                cant++;
            }
        }
    }

    /*******************************************************************************************************
     * seTraslapaConOtros
     *******************************************************************************************************/

    private boolean seTraslapaConOtros(double x, double y) {

        for (int i = 0; i < vacunatorios.size(); i++)  {

            if (vacunatorios.get(i).getTerritory().intersects(x, y, x + vacSize, y + vacSize)) {
                return true;
            }
        }

        return false;
    }


    /*******************************************************************************************************
     * getWidth
     *******************************************************************************************************/
 
    public double getWidth() {
        return width;
    }

    /*******************************************************************************************************
     * getLength
     *******************************************************************************************************/
    
    public double getLength() {
        return length;
    }

    /*******************************************************************************************************
     * getCantIndividuos
     *******************************************************************************************************/
   
    public int getCantIndividuos() {
        return cantIndividuos;
    }

    /*******************************************************************************************************
     * getCantInfectados
     *******************************************************************************************************/
    
    public int getCantInfectados() {
        return cantInfectados;
    }

    /*******************************************************************************************************
     * getITime
     *******************************************************************************************************/

    public int getITime() {
        return iTime;
    }

    /*******************************************************************************************************
     * getRTime
     *******************************************************************************************************/
    
    public int getRTime() {
        return rTime;
    }

    /*******************************************************************************************************
     * getSpeed
     *******************************************************************************************************/
    
    public double getSpeed() {
        return speed;
    }

    /*******************************************************************************************************
     * getDeltaAngle
     *******************************************************************************************************/
    
    public double getDeltaAngle() {
        return deltaAngle;
    }

    /*******************************************************************************************************
     * getDistanciaContagio
     *******************************************************************************************************/
    
    public int getDistanciaContagio() {
        return distanciaContagio;
    }

    /*******************************************************************************************************
     * getFraccionUsoMask
     *******************************************************************************************************/
    
    public double getFraccionUsoMask() {
        return fraccionUsoMask;
    }

    /*******************************************************************************************************
     * getP0
     *******************************************************************************************************/
    
    public double getP0() {
        return p0;
    }

    /*******************************************************************************************************
     * getP1
     *******************************************************************************************************/
    
    public double getP1() {
        return p1;
    }

    /*******************************************************************************************************
     * getP2
     *******************************************************************************************************/
    
    public double getP2() {
        return p2;
    }

    /*******************************************************************************************************
     * getNumVac
     *******************************************************************************************************/
    
    public int getNumVac() {
        return numVac;
    }

    /*******************************************************************************************************
     * getVacSize
     *******************************************************************************************************/
    
    public double getVacSize() {
        return vacSize;
    }

    /*******************************************************************************************************
     * setPersonas
     *******************************************************************************************************/
    
    public void setPersonas(ArrayList<Individuo> personas) {
        this.personas = personas;
    }

    /*******************************************************************************************************
     * getPersonas
     *******************************************************************************************************/
    
    public ArrayList<Individuo> getPersonas() {
        return personas;
    }

    /*******************************************************************************************************
     * getVaculatorios
     *******************************************************************************************************/
    
    public ArrayList<Vacunatorio> getVacunatorios() {
        return vacunatorios;
    }

    /*******************************************************************************************************
     * getStateDescription
     *******************************************************************************************************/
    
    public String getStateDescription() {
        return Individuo.getStateDescription();
    }

    /*******************************************************************************************************
     * getState
     *******************************************************************************************************/
    
    public String getState() {

        int[] result = cantPersonasPorEstadoSalud();

        return result[Individuo.VACUNADO] + ";" + result[Individuo.INFECTADO] + ";" + result[Individuo.RECUPERADO] + ";" + result[Individuo.SUSCEPTIBLE];
    }

    //
    //
    //

    private int[] cantPersonasPorEstadoSalud() {

        int[] result = new int[4];

        result[Individuo.VACUNADO]    = 0;
        result[Individuo.INFECTADO]   = 0;
        result[Individuo.RECUPERADO]  = 0;
        result[Individuo.SUSCEPTIBLE] = 0;

        for (int i = 0; i < personas.size(); i++) {
            result[personas.get(i).getEstadoSalud()]++;
        }

        return result;
    }
}


import java.io.PrintStream;

import java.math.BigDecimal;

import java.text.DecimalFormatSymbols;
import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.Locale;



public class Simulador {

    /** TODO_javadoc. */
    private Comuna comuna = null;

    /** TODO_javadoc. */
    private PrintStream out = null;

    /*******************************************************************************************************
     * Simulador
     *******************************************************************************************************/
    
    public Simulador(PrintStream output, Comuna comuna) {

        out         = output;
        this.comuna = comuna;

        this.comuna.setPersonas(genPersonas(comuna.getCantIndividuos(), comuna.getCantInfectados(), comuna.getFraccionUsoMask()));
    }

    /*******************************************************************************************************
     * genPersonas
     *******************************************************************************************************/
    
    private ArrayList<Individuo> genPersonas(int cantIndividuos, int cantInfectados, double fraccionUsoMask) {

        ArrayList<Individuo> personas = new ArrayList<Individuo>();

        int cantUsaMask = (int) ((cantIndividuos - cantInfectados) * fraccionUsoMask);

        for (int i = 0; i < cantIndividuos - cantInfectados; i++) {  // sustraccion mayor prioridad que relacional
            personas.add(new Individuo(comuna, i,                                   Individuo.SUSCEPTIBLE, i < cantUsaMask));
        }

        //

        cantUsaMask = (int) (cantInfectados * fraccionUsoMask);

        for (int i = 0; i < cantInfectados; i++) {
            personas.add(new Individuo(comuna, cantIndividuos - cantInfectados + i, Individuo.INFECTADO,   i < cantUsaMask));
        }

        //

        return personas;
    }

    /*******************************************************************************************************
     * simulate
     *******************************************************************************************************/
    
    public void simulate(double delta_t, double end_time, double sampling_time, int vacTime) {

        BigDecimal deltaT       = new BigDecimal(delta_t);
        BigDecimal endTime      = new BigDecimal(end_time);
        BigDecimal samplingTime = new BigDecimal(sampling_time);
        BigDecimal t            = BigDecimal.ZERO;

        printStateDescription();
        printState(t.doubleValue());

        while (t.compareTo(endTime) < 0) {

            // generar vacunatorios al instante vacTime (verifica que no haya alguno, pues se generaron antes)

            if (comuna.getVacunatorios().size() == 0 && t.intValue() >= vacTime) {
                comuna.genVacunatorios();
            }

            for (BigDecimal nextStop = t.add(samplingTime) ; t.compareTo(nextStop) < 0; t = t.add(deltaT)) {
                comuna.computeNextState(t.doubleValue(), delta_t);
            }

            printState(t.doubleValue());
        }
    }

    /*******************************************************************************************************
     * printStateDescription
     *******************************************************************************************************/
    
    private void printStateDescription() {
        out.println("Time;" + comuna.getStateDescription());
    }

    /*******************************************************************************************************
     * printState
     *******************************************************************************************************/
    
    private void printState(double t) {
        out.println((int) t + ";" + comuna.getState());
    }

}


import java.util.HashMap;
import java.util.Iterator;

import java.util.Locale;



public class Individuo {

    /** TODO_javadoc. */
    public static final int VACUNADO = 0;

    /** TODO_javadoc. */
    public static final int INFECTADO = 1;

    /** TODO_javadoc. */
    public static final int RECUPERADO = 2;

    /** TODO_javadoc. */
    public static final int SUSCEPTIBLE = 3;

    //

    /** TODO_javadoc. */
    private Comuna comuna = null;

    /** TODO_javadoc. */
    private int index;

    /** TODO_javadoc. */
    private int estadoSalud;

    /** TODO_javadoc. */
    private boolean usaMask;

    //

    /** TODO_javadoc. */
    private double x;

    /** TODO_javadoc. */
    private double y;

    /** TODO_javadoc. */
    private double angle;

    /** TODO_javadoc. */
    private double timeInicioEstadoSaludActual = 0.0;

    /** TODO_javadoc. */
    public HashMap<Integer, Double> contactos = null;

    /*******************************************************************************************************
     * Individuo
     *******************************************************************************************************/
    
    public Individuo(Comuna comuna, int index, int estadoSalud, boolean usaMask) {

        this.comuna      = comuna;
        this.index       = index;
        this.estadoSalud = estadoSalud;
        this.usaMask     = usaMask;

        x         = Math.random() * comuna.getWidth();  // posicion inicial
        y         = Math.random() * comuna.getLength(); // posicion inicial
        angle     = Math.random() * 2 * Math.PI;        // angulo inicial
        contactos = new HashMap<Integer, Double>();     // lista de contactos cercanos por indice
    }

    /*******************************************************************************************************
     * computeNextState
     *******************************************************************************************************/
    
    public void computeNextState(double t, double delta_t) {

        switch (estadoSalud) {

        /************************************************
         *
         ************************************************/

        case INFECTADO :

            // los individuos infectados pasan a estado "R", de recuperado, luego de I_time segundos

            if (t - timeInicioEstadoSaludActual > comuna.getITime()) {

                estadoSalud = RECUPERADO;

                timeInicioEstadoSaludActual = t; // inicio nuevo estado;
            }
            else {

                // si cayo en vacunatorio

                for (int i = 0; i < comuna.getVacunatorios().size(); i++) {

                    if (comuna.getVacunatorios().get(i).estaDentro(x, y)) {

                        estadoSalud = VACUNADO;

                        timeInicioEstadoSaludActual = t; // inicio nuevo estado

                        break;
                    }
                }
            }

            break;

        /************************************************
         *
         ************************************************/

        case SUSCEPTIBLE :

            verificaContactos(t);

            if (verificaProbabilidades(t) >= 1.0) {

                estadoSalud = INFECTADO;

                timeInicioEstadoSaludActual = t; // inicio nuevo estado
            }

            break;

        /************************************************
         *
         ************************************************/

        case RECUPERADO :

            // Extra crédito: Los individuos recuperados se vuelven susceptible al cabo de un tiempo

            if (t - timeInicioEstadoSaludActual > comuna.getRTime()) {

                estadoSalud = SUSCEPTIBLE;

                timeInicioEstadoSaludActual = t; // inicio nuevo estado
            }

            break;
        }

        //
        // proxima posicion y angulo
        //

        double x_dx = x + comuna.getSpeed() * Math.cos(angle);
        double y_dy = y + comuna.getSpeed() * Math.sin(angle);

        if (x_dx < 0) {

            x_dx = -x_dx;

            angle = Math.PI - angle;
        }
        else if (x_dx > comuna.getWidth()) {

            x_dx = 2 * comuna.getWidth() - x_dx;

            angle = Math.PI - angle;
        }

        //

        if (y_dy < 0) {

            angle = 2 * Math.PI - angle;

            y_dy = -y_dy;
        }
        else if (y_dy > comuna.getLength()) {

            y_dy = 2 * comuna.getLength() - y_dy;

            angle = 2 * Math.PI - angle;
        }

        // nuevo angulo

        angle += comuna.getDeltaAngle() * (1 - 2 * Math.random());

        // nueva posicion

        x = x_dx;
        y = y_dy;
    }

    /*******************************************************************************************************
     * verificaContactos
     *******************************************************************************************************/
    
    public void verificaContactos(double t) {

        for (int i = 0; i < comuna.getPersonas().size(); i++) {

            if (i != index) { // no me considero a mi mismo

                if (comuna.getPersonas().get(i).getEstadoSalud() == Individuo.INFECTADO) {

                    double distancia = comuna.getPersonas().get(i).calculaDistancia(x, y);

                    if (distancia < comuna.getDistanciaContagio()) {

                        // verifico si ya lo tengo en contactos

                        if (!contactos.containsKey(i)) { // agregar inicio t
                            contactos.put(i, t);
                        }
                    }
                    else if (contactos.containsKey(i)) { // estaba, pero se alejo
                        contactos.remove(i);
                    }
                }
                else if (contactos.containsKey(i)) { // estaba en contactos o ya no esta infectado pero se alejo
                    contactos.remove(i);
                }
            }
        }
    }

    /*******************************************************************************************************
     * verificaProbabilidades
     *******************************************************************************************************/
    
    public double verificaProbabilidades(double t) {

        double p_total = 0;

        for (Iterator<Integer> iter = contactos.keySet().iterator(); iter.hasNext(); ) {

            int idx = iter.next();

            double p = comuna.getP0();

            if (comuna.getPersonas().get(idx).isUsaMask() && usaMask /* yo */) {
                p = comuna.getP2();
            }
            else if (comuna.getPersonas().get(idx).isUsaMask() || usaMask /* yo */) {
                p = comuna.getP1();
            }

            p_total += (int) (t - contactos.get(idx)) * p; // sumo la cantidad de segs que llevo
        }

        return p_total;
    }

    /*******************************************************************************************************
     * verificaContactos
     *******************************************************************************************************/
    
    public double calculaDistancia(double x1, double y1) {

        double dx = x1 -x;
        double dy = y1 -y;

        return Math.sqrt(dx * dx + dy * dy);
    }

    //
    //
    //

    /*******************************************************************************************************
     * isUsaMask
     *******************************************************************************************************/
    
    public boolean isUsaMask() {
        return usaMask;
    }

    /*******************************************************************************************************
     * getEstadoSalud
     *******************************************************************************************************/
    
    public int getEstadoSalud() {
        return estadoSalud;
    }

    /*******************************************************************************************************
     * getStateDescription
     *******************************************************************************************************/
    
    public static String getStateDescription() {
        return "Vac;Inf;Rec;Sus";
    }
}

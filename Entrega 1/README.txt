Archivos que componen la Tarea1-POO: Comuna.java Individuo.java Pandemia.java Simulador.java Vacunatorio.java

Pasos a seguir para compilar en cmd:
Windows:
cd Direccion_carpeta_codigos   (ejemplo: C:\Users\seba\Desktop\Tarea1-POO\Tarea1)


javac Comuna.java Individuo.java Pandemia.java Simulador.java Vacunatorio.java

java Pandemia Numeros.txt > archivo.csv
(sin el "> archivo.csv" lo imprimirá por consola)

Este programa tiene como salida la descripcion general del problema, el cual arroja el Tiempo transcurrido, los vacunados, infectados y el punto 
extra que son los susceptibles.

Cabe destacar que nos percatamos a ultima hora de que las salidas debian ser por etapa del problema, pero estas estan 
integradas como una unica salida, como lo dice la descripcion general de la tarea.

*Optamos por desarrollar la bonificación extra.*

Archivos que componen la Tarea2-POO: Comuna.java ComunaView.java MAKEFILE Numeros.txt Pedestrian.java PedestrianView.java Simulator.java SimulatorConfig.java SimulatorMenuBar.java Stagex.java
(Dicha X va a depender de la stage que ejecute)
Pasos a seguir para compilar en cmd:
Windows:
cd Direccion_carpeta_codigos   (ejemplo: C:\Users\seba\Desktop\Tarea2-POO\Tarea2\src)

javac *.java

java StageX Numeros.txt  (X va a depender de la Stage ejecutada)

Este programa al ejecutarlo muestra una interfaz, que contiene lo solicitado dependiendo de la stage.
Para empezar la simulacion se debe ir a Control -> Start y esta comenzara a ejecutarse a tiempo real,
para acelerar o ralentizar la simulacion se debe usar las flecha del teclado, derecha para acelerar,
izquierda para retroceder. En Control->Stop se pausara la simulacion y seguira asi hasta que vuelva
a empezar la simulacion nuevamente, finalmente si esta se esta ejecutando y presionas Control->Start
esta se reiniciara completamente.

Representacion de los puntos y sus colores:
Cuadrado azul: Susceptibles(S)
Circulo rojo con variaciones de intensidad: (Infectados) -> La variacion de la intensidad va a depender
de cuanto tiempo este pasa infectado y se va recuperando a medida que pase el tiempo.
Cuadrado cafe: Recuperados(R)
Cuadrado verde: Vacunados (V)
Si dichos puntos tienen un borde negro, entonces quiere decir que usan máscara, de lo contrario
dice que no usan máscara.
Área verde: zonas de vacunación



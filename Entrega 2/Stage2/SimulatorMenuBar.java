
import javafx.event.ActionEvent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

public class SimulatorMenuBar extends MenuBar {
    SimulatorMenuBar (Simulator simulator){
        Menu controlMenu = new Menu("Control");
        getMenus().add(controlMenu);
        MenuItem start = new MenuItem("Start");
        MenuItem stop = new MenuItem("Stop");
        controlMenu.getItems().addAll(start, stop);
        start.setOnAction(e ->{simulator.start();});
        stop.setOnAction(e ->{simulator.stop();});
        
        Menu controlMenu2 = new Menu("Settings");
        getMenus().add(controlMenu2);
        MenuItem ChangeN = new MenuItem("Change Npersons");
        MenuItem ChangeI = new MenuItem("Change Ipersons");
        controlMenu2.getItems().addAll(ChangeN, ChangeI);
        
        
    }
}


import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;

public class PedestrianView {
    private Pedestrian person;
    private Rectangle view;
    private Rectangle view2;
    private final double SIZE = 5;
    public PedestrianView(Comuna comuna, Pedestrian p, int a) {
        if(a == 0){
            person = p;
            view = new Rectangle(SIZE, SIZE, Color.BLUE);
            view.setX(person.getX()-SIZE/2);   // Rectangle x position is the X coordinate of the
            // upper-left corner of the rectangle
            view.setY(person.getY()-SIZE/2); // Rectangle y position is the Y coordinate of the
            // upper-left corner of the rectangle
            comuna.getView().getChildren().add(view);

        }else if(a == 1){
            person = p;
            view = new Rectangle(SIZE, SIZE, Color.RED);
            view.setArcHeight(5);
            view.setArcWidth(5);
            view.setX(person.getX()-SIZE/2);   // Rectangle x position is the X coordinate of the
            // upper-left corner of the rectangle
            view.setY(person.getY()-SIZE/2); // Rectangle y position is the Y coordinate of the
            // upper-left corner of the rectangle
            comuna.getView().getChildren().add(view);
        }else if(a == 2){
            person = p;
            view = new Rectangle(SIZE, SIZE, Color.BROWN);
            view.setX(person.getX()-SIZE/2);   // Rectangle x position is the X coordinate of the
            // upper-left corner of the rectangle
            view.setY(person.getY()-SIZE/2); // Rectangle y position is the Y coordinate of the
            // upper-left corner of the rectangle
            comuna.getView().getChildren().add(view);
        }
    }
    public void update() {
        view.setX(person.getX());
        view.setY(person.getY());
    }

    public void updateBlue(){
        view.setFill(Color.BLUE);
    }
    public void updateRed(){
        view.setFill(Color.RED);
        view.setArcHeight(5);
        view.setArcWidth(5);
    }
    public void updateBrown(){
        view.setFill(Color.BROWN);
        view.setArcHeight(0);
        view.setArcWidth(0);
    }
    
}



public class Pedestrian {
    private double x, y, speed, angle, deltaAngle;
    private double x_tPlusDelta, y_tPlusDelta;
    private Comuna comuna;
    private PedestrianView pedestrianView;
    private int a;
    private double timeInfect;
    private boolean UsaMask;

    public Pedestrian(Comuna comuna, double speed, double deltaAngle, int a, boolean UsaMask){
        this.comuna = comuna;
        this.speed = speed;
        this.deltaAngle=deltaAngle;
        x = Math.random()*comuna.getWidth();
        y = Math.random()*comuna.getHeight();
        angle = Math.random()*2*Math.PI;
        this.a = a;
        this.timeInfect = 0;
        this.UsaMask = UsaMask;
        pedestrianView = new PedestrianView(comuna, this, a, UsaMask);

    }
    public double getX(){
        return x;
    }
    public double getY() {
        return y;
    }
    public void computeNextState(double delta_t) {
        double r=Math.random();
        angle+=deltaAngle*(1-2*r);
        x_tPlusDelta=x+speed*Math.cos(angle)*delta_t;
        y_tPlusDelta=y+speed*Math.sin(angle)*delta_t;
        if(x_tPlusDelta < 0){   // rebound logic
            x_tPlusDelta=-x_tPlusDelta;
            angle=Math.PI-angle;
        }
        if(y_tPlusDelta < 0){
            y_tPlusDelta=-y_tPlusDelta;
            angle=2*Math.PI-angle;
        }
        if( x_tPlusDelta > comuna.getWidth()){
            x_tPlusDelta=2*comuna.getWidth()-x_tPlusDelta;
            angle=Math.PI-angle;
        }
        if(y_tPlusDelta > comuna.getHeight()){
            y_tPlusDelta=2*comuna.getHeight()-y_tPlusDelta;
            angle=2*Math.PI-angle;
        }
    }
    public void updateState(){
        x=x_tPlusDelta;
        y=y_tPlusDelta;
    }
    public void updateView() {
        pedestrianView.update();
    }
    public void respawn(){
        this.x = Math.random()*comuna.getWidth();
        this.y = Math.random()*comuna.getHeight();
    }

    public double calculaDistancia(double x1, double y1) {

        double dx = x1 -x;
        double dy = y1 -y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    public int getStatus(){
        return a;
    }

    public boolean getUsaMask(){
        return UsaMask;
    }

    public void timeInf(double delta_t){
        this.timeInfect = timeInfect + delta_t;
    }
    public double getTimeInf(){
        return timeInfect;
    }

    public void setStatus(int a){
        this.a = a;
        if(a == 0){
            pedestrianView.updateBlue();
        }else if(a == 1){
            pedestrianView.updateRed();
        }else if(a == 2){
            pedestrianView.updateBrown();
        }
    }
}

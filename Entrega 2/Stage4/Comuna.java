
import java.util.ArrayList;
import javafx.scene.paint.Color;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.layout.Pane;

public class Comuna {
    private ArrayList <Pedestrian> person;
    private Rectangle2D territory; // Alternatively: double width, length;
                                   // but more methods would be needed.
    private ComunaView view;
    private Pane graph;
    
    
    public Comuna(){
        double width = SimulatorConfig.WIDTH;
        double length = SimulatorConfig.LENGTH;
        territory = new Rectangle2D(0,0, width, length);
        double speed = SimulatorConfig.SPEED;
        double deltaAngle = SimulatorConfig.DELTA_THETA;
        view = new ComunaView(this); // What if you exchange this and the follow line?
        person =new ArrayList <Pedestrian>();
        graph = new Pane();  // to be completed in other stages.
        
    }
   
    public double getWidth() {
        return territory.getWidth();
    }
    public double getHeight() {
        return territory.getHeight();
    }
    public void computeNextState (double delta_t) {
        for (int i = 0; i < person.size(); i++) {
            person.get(i).computeNextState(delta_t);
        }
    }
    public void updateState () {
        for (int i = 0; i < person.size(); i++) {
            person.get(i).updateState();
        }
    }
    public void updateView(){
        for (int i = 0; i < person.size(); i++) {
            person.get(i).updateView();
        }
        view.update();
    }
    public void setPedestrian(ArrayList<Pedestrian> person) {
        this.person = person;
    }
    public ArrayList<Pedestrian> getPedestrian() {
        return person;
    }
    public void geetPedestrian(ArrayList<Pedestrian> person){
        for (int i = 0; i < person.size(); i++){
            person.get(i).respawn();
        }
    }
    public double getCantIndividuos() {
        return SimulatorConfig.N;
    }
    public double getCantInfectados() {
        return SimulatorConfig.I;
    }
    public double getDistanciaContagio() {
        return SimulatorConfig.D;
    }
    public double getI_Time(){
        return SimulatorConfig.I_TIME;
    }
    public double getSpeed() {
        return SimulatorConfig.SPEED;
    }
    public double getAngulo() {
        return SimulatorConfig.DELTA_THETA;
    }
    public Group getView() {
        return view;
    }
    public Pane getGraph(){
        return graph;
    }

    public void verificaRecuperados(){
        for (int i = 0; i < person.size(); i += 1) {
            if (person.get(i).getStatus() == 1) {
                if(person.get(i).getTimeInf()>=getI_Time()){
                    person.get(i).setStatus(2);
                }
                person.get(i).timeInf(SimulatorConfig.DELTA_T);
            }
        }
    }

    public void verificaContacto(){
        for (int i = 0; i < person.size(); i ++) {
            if(person.get(i).getStatus() == 1) {
                for (int j = 0; j < person.size(); j ++) {
                    if((person.get(j).getStatus()) == 0) {
                        double distancia = Math.sqrt((Math.pow(person.get(j).getX() - person.get(i).getX(), 2)) + (Math.pow(person.get(j).getY() - person.get(i).getY(), 2)));
                        if(distancia <= SimulatorConfig.D) {
                            double chance = Math.random();
                            if((person.get(i).getUsaMask()==false)&&(person.get(i).getUsaMask()==false)){
                                if(chance < SimulatorConfig.P0) {
                                    person.get(j).setStatus(1);
                                }
                            }else if ((person.get(i).getUsaMask()==true)&&(person.get(i).getUsaMask()==false)){
                                if(chance < SimulatorConfig.P1) {
                                    person.get(j).setStatus(1);
                                }
                            }else if ((person.get(i).getUsaMask()==true)&&(person.get(i).getUsaMask()==true)){
                                if(chance < SimulatorConfig.P2) {
                                    person.get(j).setStatus(1);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void verificaVacunado(){
        for(int i=0;i<person.size();i+=1){
            if(person.get(i).getStatus() == 0){
                for(int j=0;j<view.getArrayVacs().size();j+=1){
                    if((view.getArrayVacs().get(j).getX()+SimulatorConfig.VAC_SIZE)>person.get(i).getX()){
                        if(person.get(i).getX()>view.getArrayVacs().get(j).getX()){
                            if((view.getArrayVacs().get(j).getY()+SimulatorConfig.VAC_SIZE)>person.get(i).getY()){
                                if(person.get(i).getY()>view.getArrayVacs().get(j).getY()){
                                    person.get(i).setStatus(3);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

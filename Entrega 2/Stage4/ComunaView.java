
import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class ComunaView extends Group {
    private final Comuna comuna;
    private Rectangle viewVac;
    private ArrayList<Rectangle> viewVacs;

    public ComunaView(Comuna c){
        this.viewVacs =new ArrayList<>();
        comuna = c;
        Rectangle territoryView = new Rectangle(comuna.getWidth(), comuna.getHeight(), Color.WHITE);
        territoryView.setStroke(Color.BROWN);
        getChildren().add(territoryView);
        setFocusTraversable(true);  // needed to receive mouse and keyboard events.} 
        double SIZE = SimulatorConfig.VAC_SIZE;
        for(int i=0;i<SimulatorConfig.NUM_VAC;i+=1){
            viewVac = new Rectangle(SIZE, SIZE, Color.GREENYELLOW);
            viewVac.setX(Math.random()*(SimulatorConfig.WIDTH-SimulatorConfig.VAC_SIZE));
            viewVac.setY(Math.random()*(SimulatorConfig.LENGTH-SimulatorConfig.VAC_SIZE));
            getChildren().add(viewVac);
            viewVacs.add(viewVac);
        }
    }

    public void vacView(){
        double SIZE = SimulatorConfig.VAC_SIZE;

        for(int i=0;i<SimulatorConfig.NUM_VAC;i+=1){
            viewVac = new Rectangle(SIZE, SIZE, Color.GREENYELLOW);
            viewVac.setX(Math.random()*(SimulatorConfig.WIDTH-SimulatorConfig.VAC_SIZE));
            viewVac.setY(Math.random()*(SimulatorConfig.LENGTH-SimulatorConfig.VAC_SIZE));
            getChildren().add(viewVac);
            viewVacs.add(viewVac);
        }
    }

    public ArrayList<Rectangle> getArrayVacs(){
        return viewVacs;
    }

    public void update(){
    }
}
